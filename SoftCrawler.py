# -*- coding: utf-8 -*-
import math
import time
import os

from sys import version_info

if version_info.major < 3:
    print('Python 3+ required.')
    exit(1)
if os.name != 'nt':
    print('Windows support only.')
    exit(1)

import argparse
import codecs
import _thread
from threading import Thread, Timer
import pythoncom

import yaml
import wmi
from wmi import pywintypes

from colorama import Fore, init


computers = []
report = []
trash = []
info = []
error = {}
script_dir = os.path.dirname(os.path.realpath(__file__))+'/'


def find_function(fn_name, parent_id):
    print('Timeout in %s. Use -v to find stuck thread. Parent thread id: %s.' % (fn_name, parent_id))
    os._exit(1)  # cheat


def timeout(s):
    def outer(fn):
        def inner(*args, **kwargs):
            timer = Timer(s, find_function, args=[fn.__name__, _thread.get_ident()])
            timer.daemon = True
            timer.start()
            timer.join(3)
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result

        return inner
    return outer


def read_file(file):
    result = open(file).readlines()
    return [item.encode('cp1251').decode('utf8').strip() for item in result]


def get_os(connector):
    result = []
    for item in connector.query('Select Caption from Win32_OperatingSystem'):
        result.append(item.Caption)

    return ', '.join(result)


def get_user(connector):
    result = []
    for item in connector.query('Select UserName from Win32_ComputerSystem'):
        if item.UserName is None:
            result.append('n/a')
        else:
            result.append(item.UserName)

    return ', '.join(result)


@timeout(400)
def get_soft(connector):
    result = []
    # Select Caption, Description, Name, ProductName, Vendor, Version, LastUse from Win32_SoftwareFeature
    for item in connector.query('Select ProductName from Win32_SoftwareFeature'):
        if item.ProductName is None:
            item.ProductName = 'n/a'
        if item.ProductName not in result:
            result.append(item.ProductName)

    return result


def timer(function):
    def wrapped():
        start_time = time.time()
        function()
        print("%s ended in %s seconds." % (os.path.basename(__file__), (time.time() - start_time)))
    return wrapped


def gen_report(computers, username, password, skip_soft, trash_soft, max_threads, verbose):
    global report, error, trash, info, script_dir

    print('Hosts: %d.' % len(computers))

    def chunks(l, n):
        result = []
        for i in range(0, len(l), n):
            result.append(l[i:i + n])
        return result

    def worker(num, part):
        pythoncom.CoInitialize()  # cheat
        if verbose:
            print('Thread-%s (%s): part = %s' % (num, _thread.get_ident(), ', '.join(part)))
        for computer in part:
            try:

                connector = wmi.WMI(
                    wmi=wmi.connect_server(
                        computer,
                        user=username,
                        password=password,
                        security_flags=0x80  # if 0, connect will wait forever; if 0x80, connect will timeout at 2 mins
                    )
                )

                uname = get_user(connector)
                if verbose:
                    print(computer, uname)

                os = get_os(connector)
                if verbose:
                    print(computer, os)

                soft = get_soft(connector)

                for application in soft:
                    report_line = '%s;%s;%s;%s' % (computer, uname, application, os)
                    output_line = '%s\t%s%s' % (computer, uname.ljust(30, ' '), application)  # красивый вывод в консоль

                    try:
                        report.append(report_line)  # full report
                        if any(a.lower() in application.lower() for a in skip_soft):
                            continue
                        else:
                            if any(a.lower() in application.lower() for a in trash_soft):
                                trash.append(report_line)
                                print(Fore.RED + output_line + Fore.RESET)
                            else:
                                info.append(report_line)
                                print(Fore.YELLOW + output_line + Fore.RESET)
                    except (UnicodeEncodeError, AttributeError) as err:
                        error.update({computer: str(err)})

            except (wmi.x_wmi, pywintypes.com_error) as err:
                error.update({computer: str(err)})
                continue
        if verbose:
            print('Thread-%s: stopped.' % num)

        return

    if len(computers) > max_threads:
        chunks_total = max_threads
    else:
        chunks_total = len(computers)

    print('Threads: %d.' % chunks_total)

    chunk = chunks(computers, math.ceil(len(computers)/chunks_total))

    threads = []
    for x in range(len(chunk)):
        thread = Thread(target=worker, args=((x+1), chunk[x],))
        threads.append(thread)
        thread.start()
        time.sleep(.1)

    for thread in threads:
        if thread.is_alive():
            thread.join()

    print('Scan has been completed.')

    if len(report) > 0:
        print('Generating reports.')
        # TODO: try PermissionError ?
        file_full = codecs.open(script_dir + 'report_full.csv', 'w', 'utf8')
        file_trash = codecs.open(script_dir + 'report_trash.csv', 'w', 'utf8')
        file_info = codecs.open(script_dir + 'report_info.csv', 'w', 'utf8')

        try:
            for line in report:
                file_full.write(line+'\n')
            for line in trash:
                file_trash.write(line+'\n')
            for line in info:
                file_info.write(line+'\n')
        except (UnicodeEncodeError, AttributeError) as err:
            print('Error writing file', err)

        print('Reports created.')
    else:
        print('Reports empty.')

    if len(error) > 0:
        file_error = codecs.open(script_dir + 'error.txt', 'w', 'utf8')
        for key, value in error.items():
            file_error.write('%s: %s\r\n' % (key, value))
        print('Error count: %d.' % len(error))
        print('Error report created.')


@timer
def main():
    init()  # color output

    argparser = argparse.ArgumentParser()

    argparser.add_argument('-g', '--config', type=str, required=False, default=script_dir + 'SoftCrawler.conf',
                           help='your config file with username and password, '
                                'by DEFAULT using SoftCrawler.conf inside running directory')
    argparser.add_argument('-c', '--computers', type=str, default=script_dir + 'computers.txt',
                           help='your hostname/ip list, '
                                'by DEFAULT using computers.txt inside running directory')
    argparser.add_argument('-skip', '--skip', type=str, default=script_dir + 'skip.txt',
                           help='your application skip list, '
                                'by DEFAULT using skip.txt inside running directory')
    argparser.add_argument('-trash', '--trash', type=str, default=script_dir + 'trash.txt',
                           help='your application trash list, '
                                'by DEFAULT using trash.txt inside running directory')
    argparser.add_argument('-v', '--verbose', action='store_true', default=False,
                           help='verbose output')  # TODO: logger support
    argparser.add_argument('-t', '--threads', type=int, default=20,
                           help='max threads, DEFAULT is 20.')

    args = argparser.parse_args()
    with open(args.config, 'r') as cfg:
        config = yaml.load(cfg)

    gen_report(
        computers=read_file(args.computers),
        username=config['username'],
        password=config['password'],
        skip_soft=read_file(args.skip),
        trash_soft=read_file(args.trash),
        max_threads=args.threads,
        verbose=args.verbose
    )


if __name__ == "__main__":
    main()
