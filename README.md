# [SoftCrawler.py](https://gitlab.com/iMorozov/SoftCrawler)

SoftCrawler.py - command line utility that provides remote scan Windows based workstations for installed software.

**Windows support only.**

## Requirements:
```
py -3.5 -m pip install wmi
py -3.5 -m pip install PyYaml
py -3.5 -m pip install colorama
```